//
//  ViewController.swift
//  iosZero1
//
//  Created by arodriguez on 12-04-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth


class LoginViewController: UIViewController {
 
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    let model: LoginModelLogic = LoginModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func didTapLogin(_ sender: Any) {
        self.model.login(email: email.text ?? "", password: password.text ?? "") { (response) in
            if response.success {
                if let user = response.payload {
                    print(user)
                }
                self.performSegue(withIdentifier: "gotoDashboard", sender: nil)
            } else {
                self.showAlert(title: "Ups", message: response.message)
            }
        }
    }
}

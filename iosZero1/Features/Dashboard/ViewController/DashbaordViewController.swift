//
//  DashbaordViewController.swift
//  iosZero1
//
//  Created by arodriguez on 12-04-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth
class DashbaordViewController: UIViewController {

    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var userId: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = Auth.auth().currentUser {
            email.text = user.email!
            userId.text = user.uid
        }
    }
    
    @IBAction func didTapClose(_ sender: Any) {    
        self.dismiss(animated: true)
    }

}

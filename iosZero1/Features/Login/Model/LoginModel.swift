//
//  xViewController.swift
//  iosZero1
//
//  Created by arodriguez on 16-04-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import UIKit
import FirebaseAuth

struct Response {
    let success: Bool
    let message: String
    let payload: User?
    
    init(success: Bool, message: String, payload: User? = nil) {
        self.success = success
        self.message = message
        self.payload = payload
    }
}

protocol LoginModelLogic {
    func login(email : String, password: String, completion: @escaping (Response) -> Void)
}

class LoginModel: LoginModelLogic {

    func login(email : String, password: String, completion: @escaping (Response) -> Void) {
    
        if email == "" {
            completion(Response(success: false, message: "Ingresa tu email"))
            return
        }

        if password == "" {
          completion(Response(success: false, message: "Ingresa tu contraseña"))
          return
        }

        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
              if error == nil {
                completion(Response(success: true, message: "", payload: result?.user ))
              } else {
                completion(Response(success: false, message: error?.localizedDescription ?? ""))
              }
        }
    }
}

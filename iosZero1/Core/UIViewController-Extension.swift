//
//  UIViewController-Extension.swift
//  iosZero1
//
//  Created by arodriguez on 12-04-20.
//  Copyright © 2020 arodriguez. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
           let str = "\(title) - \(message)"
           print(str)
           
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           let btn = UIAlertAction(title: "OK", style: .default, handler: nil)
           
           alert.addAction(btn)
           
           self.present(alert, animated: true, completion: nil)
    }
}
